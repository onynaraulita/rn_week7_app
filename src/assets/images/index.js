import Logo from './logo.png';
import SplashBackground from './SplashBackground.png';
import ImageHeader from './header.png';
import LogoUvi from './LogoUvi.png';
export {Logo, SplashBackground, ImageHeader, LogoUvi};
