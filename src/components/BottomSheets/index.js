import React, {Component} from 'react';
import {View, Button, Text} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import axios from 'axios';

export default class CompBottomSheets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMovieDetail: {},
      isLoading: false,
      dataGenres: [],
      dataCast: [],
      isFav: false,
      currentPage: 0,
      offset: 1,
      loadingCast: true,
      DataSource: [],
    };
  }

  async componentDidMount() {
    console.log('masuk');
    this.setState({isLoading: true});
    const dataMovieDetailDariServer = await axios.get(
      `http://code.aldipee.com/api/v1/movies/${this.props.route.params.namaFilm}`,
    );

    this.setState({dataCast: dataMovieDetailDariServer.data.credits.cast});
    this.setState({isLoading: false});
  }
  render() {
    return (
      <View>
        <Button title="See All Cast" onPress={() => this.RBSheet.open()} />
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={300}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
            },
          }}>
          <Text>Actors/Artists</Text>
          <View>
            <Text>
              {this.state.dataCast.slice(0, 6).map(item => (
                <View key={item.id} style={styles.cast}>
                  <Image
                    style={styles.gambarFilm}
                    source={{
                      uri: `${item.profile_path}`,
                    }}
                  />
                  <Button
                    title={item.name}
                    onPress={() => {
                      Linking.openURL(
                        `https://www.google.com/search?q=${item.name}`,
                      );
                    }}></Button>
                </View>
              ))}
            </Text>
          </View>
        </RBSheet>
      </View>
    );
  }
}
