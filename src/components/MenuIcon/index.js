import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {WARNA_UTAMA, WARNA_DISABLE, WARNA_KUNING} from '../../utils/constant';

const IconHomeActive = () => {
  return <Icon name="home" color={WARNA_KUNING} size={24} />;
};

const IconHomeNonActive = () => {
  return <Icon name="home" color={WARNA_DISABLE} size={24} />;
};

const IconListActive = () => {
  return <Icon name="list-ul" color={WARNA_KUNING} size={24} />;
};

const IconListNonActive = () => {
  return <Icon name="list-ul" color={WARNA_DISABLE} size={24} />;
};

const IconAccountActive = () => {
  return <Icon name="user" color={WARNA_KUNING} size={24} />;
};

const IconAccountNonActive = () => {
  return <Icon name="user" color={WARNA_DISABLE} size={24} />;
};

export {
  IconHomeActive,
  IconHomeNonActive,
  IconListActive,
  IconListNonActive,
  IconAccountActive,
  IconAccountNonActive,
};
