import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  Button,
  Share,
  TouchableOpacity,
  ScrollView,
  Linking,
} from 'react-native';

import {
  WARNA_ABU_ABU,
  WARNA_SEKUNDER,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_HITAM,
  WARNA_ABUGELAP,
} from '../../utils/constant';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import {Image} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import RBSheet from 'react-native-raw-bottom-sheet';

export default class HeaderIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMovieDetail: {},
      isLoading: false,
      dataGenres: [],
      dataCast: [],
      isFav: false,
      currentPage: 0,
      offset: 1,
      loadingCast: true,
      DataSource: [],
      listFavMovies: [],
    };
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: `Watch ${this.state.dataMovieDetail.original_title} now at MovieApp! See the movie detail at ${this.state.dataMovieDetail.homepage}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    {
      console.log('print id' + this.state.props);
    }
    return (
      <>
        <TouchableOpacity
          style={{marginRight: 20}}
          onPress={() => {
            this.setState({isFav: !this.state.isFav});

            // this.state.listFavMovies.push(this.props.id);
            // {
            //   console.log('print id' + this.props.id);
            // }
          }}>
          {this.state.isFav === true ? (
            <Icon name="heart" size={20} color="red" title="favorite" />
          ) : (
            <Icon
              name="heart"
              size={20}
              color={WARNA_ABU_ABU}
              title="favorite"
            />
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{marginRight: 20}}
          onPress={() => {
            {
              this.onShare();
            }
          }}>
          <Icon
            name="share-alt"
            size={20}
            color={WARNA_ABU_ABU}
            title="favorite"
          />
        </TouchableOpacity>
      </>
    );
  }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  detailMovie: {
    fontSize: 10,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginVertical: 4,
  },
  otherDetail: {
    marginHorizontal: 20,
    top: -30,
  },
  content: {
    color: WARNA_PUTIH,
    lineHeight: 26,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_KUNING,
    marginBottom: 10,
    marginTop: 10,
  },
  detailKiri: {paddingLeft: 15},
  detailKanan: {marginLeft: 14},
  containerPosterDetail: {
    backgroundColor: WARNA_HITAM,
    borderWidth: 1,
    borderColor: WARNA_KUNING,
    flex: 1,
    flexDirection: 'row',
    top: -50,
    left: 0.05 * windowWidth,
    width: 0.9 * windowWidth,
    borderRadius: 10,
    paddingVertical: 10,
    shadowColor: 'yellow',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  containerCast: {},
  castName: {
    fontSize: 10,
    color: 'white',
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 4,
  },
  namaCastSheet: {
    fontSize: 12,
    textAlign: 'center',
  },
  itemCast0: {
    width: 75,
    height: 140,
  },
  itemCast: {
    width: 75,
    height: 150,
  },
  gambarCast: {
    width: 75,
    height: 100,
    borderRadius: 4,
  },
  gambarCastBesar: {
    width: 64,
    height: 80,
    borderRadius: 4,
  },
  bottomSheetCast0: {
    flex: 1,
    width: 0.9 * windowWidth,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  bottomSheetCast: {
    flex: 1,
    width: windowWidth,
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  namaCast: {
    fontSize: 10,
  },
  cast: {
    flex: 1,

    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  backdropPath: {
    width: windowWidth,
    height: 200,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  headerTitle: {
    backgroundColor: 'white',
    width: windowWidth * 0.8,
    height: 200,
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 1,
  },
  gambarFilm: {
    width: 85,
    height: 130,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  container: {
    flex: 1,
    backgroundColor: WARNA_HITAM,
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
