import React, {Component, useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  Button,
  Share,
  TouchableOpacity,
  ScrollView,
  Linking,
} from 'react-native';

import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_ABUGELAP,
  WARNA_PUTIH,
} from '../../utils/constant';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';

import {Image} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const CompGenres = ({id}) => {
  const [dataGenres, setDataGenres] = useState([]);

  useEffect(async () => {
    const gett = await axios.get(`http://code.aldipee.com/api/v1/movies/${id}`);

    setDataGenres(gett.data.genres);
  }, []);

  return (
    <View style={styles.container}>
      <View style={{borderRadius: 5}}>
        <View style={styles.genreContainer}>
          {/*GENRES*/}

          <Text style={{marginBottom: 10}}>
            {dataGenres.map(item => (
              <View key={item.id}>
                <Text
                  style={{
                    color: WARNA_PUTIH,
                    backgroundColor: WARNA_ABUGELAP,
                    marginRight: 6,
                    paddingHorizontal: 10,
                    paddingVertical: 6,
                    borderRadius: 6,
                    marginTop: 6,
                    fontSize: 10,
                  }}>
                  {item.name}
                </Text>
              </View>
            ))}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default CompGenres;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {},
  genreContainer: {
    width: 0.5 * windowWidth,
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    marginRight: 0.02 * windowWidth,
    marginTop: 'auto',
  },
  genreItem: {
    maxWidth: 0.2 * windowWidth,
    backgroundColor: WARNA_ABUGELAP,
    padding: 8,
    borderRadius: 10,
    fontSize: 10,
    color: WARNA_PUTIH,
  },
});
