import React, {Component} from 'react';
import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

import axios from 'axios';
import {WARNA_ABU_ABU, WARNA_UTAMA} from '../../utils/constant';

const CompRefreshControl = id => {
  _onRefresh = async id => {
    //console.log('refresh: ' + id);
    setRefreshing(true);
    await axios.get(`http://code.aldipee.com/api/v1/movies/${id}`);

    setRefreshing(false);

    console.log('berhasil refresh');
    // fetchData().then(() => {
    //   this.setState({refreshing: false});
    // });
  };

  return (
    <>
      <TouchableOpacity
        onPress={() => {
          _onRefresh(id);
        }}>
        <Icon name="refresh" color={WARNA_UTAMA} size={20} />
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'pink',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CompRefreshControl;
