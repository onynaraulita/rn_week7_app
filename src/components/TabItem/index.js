import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {WARNA_UTAMA, WARNA_KUNING, WARNA_DISABLE} from '../../utils/constant';
import {
  IconAccountActive,
  IconAccountNonActive,
  IconListActive,
  IconListNonActive,
  IconHomeActive,
  IconHomeNonActive,
} from '../MenuIcon';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

const TabItem = ({isFocused, onPress, onLongPress, label}) => {
  const Icon = () => {
    if (label === 'Home')
      return isFocused ? <IconHomeActive /> : <IconHomeNonActive />;
    if (label === 'Explore')
      return isFocused ? <IconListActive /> : <IconListNonActive />;
    if (label === 'Account')
      return isFocused ? <IconAccountActive /> : <IconAccountNonActive />;
  };

  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}>
      <Icon />
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  text: isFocused => ({
    fontSize: 12,
    marginTop: 4,
    color: isFocused ? WARNA_KUNING : WARNA_DISABLE,
  }),
});
