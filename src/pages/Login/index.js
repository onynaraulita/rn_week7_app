import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {NavigationContainer} from '@react-navigation/native';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
} from '../../utils/constant';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import {CompInternetConnectionAlert} from '../../components';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMovie: [],
      isLoading: false,
      username: '',
      password: '',
    };
  }

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_KUNING);
  }

  handleButtonSubmit = () => {
    console.log('masuk');
    this.setState({username: this.state.username});
    this.setState({password: this.state.password});
    console.log('disini: ' + this.state.username + this.state.password);

    {
      this.state.username != '' && this.state.password != ''
        ? this.props.navigation.replace('MainApp', {
            username: this.state.username,
          })
        : Alert.alert('Error', 'Please enter your username and password');
    }

    console.log('yg dikirm: ' + this.state.username);
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          <Text style={styles.title}>Sign in</Text>
          <View>
            <View style={styles.form}>
              <TextInput
                style={styles.inputUsername}
                defaultValue=""
                placeholder="Username"
                name="username"
                onChangeText={text => {
                  this.setState({username: text});
                  console.log(this.state.username);
                }}
              />

              <TextInput
                style={styles.inputPassword}
                secureTextEntry={true}
                defaultValue=""
                placeholder="Password"
                name="password"
                onChangeText={text => {
                  this.setState({password: text});
                  console.log(this.state.password);
                }}
              />
              <Button
                style={styles.buttonLogin}
                title="Submit"
                color={WARNA_KUNING}
                onPress={this.handleButtonSubmit}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Login;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  buttonLogin: {
    padding: 10,
    borderRadius: 6,
  },
  container: {
    width: windowWidth,
    height: windowHeight,
    backgroundColor: WARNA_HITAM,
    padding: 20,
  },
  inputUsername: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
  },
  inputPassword: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
  },
  buttonShowMore: {
    backgroundColor: WARNA_KUNING,
    alignSelf: 'flex-start',
    color: WARNA_HITAM,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    padding: 6,
    borderRadius: 6,
  },
  infoMovie: {
    marginLeft: windowWidth * 0.02,
    color: WARNA_PUTIH,
  },
  posterLatest: {},
  recommended: {
    marginBottom: 20,
    color: WARNA_PUTIH,
    padding: 6,
  },
  recommendedItem: {
    marginRight: 5,
  },
  movieItem: {
    padding: 6,
    paddingRight: 6,
    marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    color: WARNA_PUTIH,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  layanan: {
    paddingLeft: 30,
    padding: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarFilm: {
    width: 120,
    height: 160,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
