import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  Dimensions,
  Button,
  Share,
  TouchableOpacity,
  ScrollView,
  Linking,
  RefreshControl,
} from 'react-native';

import {
  WARNA_ABU_ABU,
  WARNA_SEKUNDER,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_HITAM,
  WARNA_ABUGELAP,
} from '../../utils/constant';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import {CompRefreshControl} from '../../components';
import {CompBottomSheets} from '../../components';
import {Image} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import {ProgressiveImage, CompInternetConnectionAlert} from '../../components';
import RBSheet from 'react-native-raw-bottom-sheet';

export default class MovieDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMovieDetail: {},
      isLoading: false,
      dataGenres: [],
      dataCast: [],
      isFav: false,
      currentPage: 0,
      offset: 1,
      loadingCast: true,
      DataSource: [],
      listFavMovies: [],
      getIdfav: 0,
      refreshing: false,
    };
  }

  async componentDidMount() {
    try {
      this.setState({isLoading: true});
      const dataMovieDetailDariServer = await axios.get(
        `http://code.aldipee.com/api/v1/movies/${this.props.route.params.namaFilm}`,
      );

      // this.setState({getIdfav: this.props.route.params.listFavMovies});
      // console.log('id fav: ' + this.state.getIdfav);
      //console.log('print: ' + dataMovieDetailDariServer.data);

      this.setState({dataMovieDetail: dataMovieDetailDariServer.data});
      this.setState({dataGenres: dataMovieDetailDariServer.data.genres});

      //console.log('print genre : ' + this.state.dataGenres.name);

      this.setState({dataCast: dataMovieDetailDariServer.data.credits.cast});
      this.setState({isLoading: false});
    } catch (error) {
      console.log(error.response.data.error);
    }
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: `Watch ${this.state.dataMovieDetail.original_title} now at MovieApp! See the movie detail at ${this.state.dataMovieDetail.homepage}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  handleLoadMore = async () => {
    // her you put the logic to load some data with pagination
    // for example a service who return the data of the page "this.state.currentPage"
    let newData = await myExempleService(this.state.currentPage);

    this.setState({
      mockData: [...mockData, ...newData], // concat the old and new data together
      currentPage: this.state.currentPage + 1,
    });
  };

  getData = async () => {
    console.log('getData');
    this.setState({loading: true});

    const dataTodoDetailDariServer = await axios.get(
      `http://code.aldipee.com/api/v1/movies/${this.props.route.params.namaFilm}`,
    );

    this.setState({dataMovieDetail: dataTodoDetailDariServer.data});
    this.setState({isLoading: false});
  };

  _onRefresh = async () => {
    console.log('refresh');
    this.setState({refreshing: true});
    await axios.get(
      `http://code.aldipee.com/api/v1/movies/${this.props.route.params.namaFilm}`,
    );
    this.setState({refreshing: false});
    console.log('berhaisl refresh');
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          {/* <TouchableOpacity
              style={{marginRight: 20}}
              onPress={() => {
                this.setState({isFav: !this.state.isFav});

                // this.state.listFavMovies.push(this.state.getIdfav);
                // {
                //   console.log('isi list fav' + `${this.state.listFavMovies}`);
                // }
              }}>
              {this.state.isFav === true ? (
                <Icon name="heart" size={20} color="red" title="favorite" />
              ) : (
                <Icon
                  name="heart"
                  size={20}
                  color={WARNA_ABU_ABU}
                  title="favorite"
                />
              )}
            </TouchableOpacity> */}

          {/* 
          <CompRefreshControl id={this.state.dataMovieDetail.id} /> */}

          <View style={{borderRadius: 5}}>
            <View style={styles.kotakBesar}>
              <Image
                style={styles.backdropPath}
                source={{
                  uri: `${this.state.dataMovieDetail.backdrop_path}`,
                }}
              />
            </View>

            <View>
              <View style={styles.containerPosterDetail}>
                <View style={styles.detailKiri}>
                  <Image
                    style={styles.gambarFilm}
                    source={{
                      uri: `${this.state.dataMovieDetail.poster_path}`,
                    }}
                  />
                </View>

                <View style={styles.detailKanan}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: 'Montserrat-Bold',
                      marginBottom: 4,
                      color: WARNA_PUTIH,
                      flex: 1,
                      flexWrap: 'wrap',
                    }}>
                    {this.state.dataMovieDetail.original_title}
                  </Text>
                  <Text
                    style={{
                      fontFamily: 'Montserrat-Italic',
                      marginBottom: 4,
                      fontSize: 12,
                      color: WARNA_PUTIH,
                    }}>
                    {this.state.dataMovieDetail.tagline}
                  </Text>

                  <Text
                    style={{
                      fontFamily: 'Montserrat-Medium',
                      backgroundColor: WARNA_KUNING,
                      padding: 4,
                      borderRadius: 4,
                      alignSelf: 'flex-start',
                      marginVertical: 10,
                      fontSize: 12,
                    }}>
                    {this.state.dataMovieDetail.status}
                  </Text>

                  {this.state.dataMovieDetail.vote_average > 0 ? (
                    <>
                      <Text>
                        <Icon name="star" color={WARNA_KUNING} size={16} />
                        <Text style={styles.detailMovie}>
                          {this.state.dataMovieDetail.vote_average}
                        </Text>
                      </Text>

                      <Text style={styles.detailMovie}>
                        {this.state.dataMovieDetail.runtime} mins
                      </Text>

                      <Text style={styles.detailMovie}>
                        {moment(this.state.dataMovieDetail.release_date).format(
                          'DD MMMM YYYY',
                        )}
                      </Text>
                    </>
                  ) : null}
                </View>
              </View>

              <View style={styles.otherDetail}>
                {/*GENRES*/}
                <Text style={styles.title}>Genres</Text>
                {/* boleh kayak gini/ panggil comp genre */}

                <Text style={{marginBottom: 10}}>
                  {this.state.dataGenres.map(item => (
                    <View key={item.id}>
                      <Text
                        style={{
                          color: WARNA_PUTIH,
                          backgroundColor: WARNA_ABUGELAP,
                          marginRight: 6,
                          paddingHorizontal: 10,
                          paddingVertical: 6,
                          borderRadius: 6,
                          marginTop: 10,
                        }}>
                        {item.name}
                      </Text>
                    </View>
                  ))}
                </Text>

                <TouchableOpacity
                  style={{position: 'absolute', right: 10}}
                  onPress={() => {
                    this._onRefresh();
                  }}>
                  <Icon
                    name="refresh"
                    size={20}
                    color={WARNA_ABUGELAP}
                    title="favorite"
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  style={{position: 'absolute', right: 40}}
                  onPress={() => {
                    {
                      this.onShare();
                    }
                  }}>
                  <Icon
                    name="share-alt"
                    size={20}
                    color={WARNA_ABUGELAP}
                    title="favorite"
                  />
                </TouchableOpacity>
                {/*Synopsis*/}
                <Text style={styles.title}>Synopsis</Text>
                <Text style={styles.content}>
                  {this.state.dataMovieDetail.overview}
                </Text>

                {/*Watch Trailer*/}
                <TouchableOpacity
                  style={{position: 'absolute', right: 70}}
                  onPress={() => {
                    Linking.openURL(
                      `https://youtube.com/results?search_query=${this.state.dataMovieDetail.original_title}+ trailer`,
                    );
                  }}>
                  <Icon
                    name="youtube"
                    size={20}
                    color={WARNA_ABUGELAP}
                    title="video"
                  />
                </TouchableOpacity>

                {/*CAST*/}
                <Text style={styles.title}>Actors/Artists</Text>
                <View style={styles.bottomSheetCast0}>
                  {this.state.dataCast.slice(0, 8).map(item => (
                    <View key={item.id} style={styles.itemCast0}>
                      <Image
                        style={styles.gambarCastBesar}
                        source={{
                          uri: `${item.profile_path}`,
                        }}
                      />
                      <TouchableOpacity
                        onPress={() => {
                          Linking.openURL(
                            `https://www.google.com/search?q=${item.name}`,
                          );
                        }}>
                        <Text style={styles.castName}>
                          {item.original_name}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>

                <Button
                  color={WARNA_KUNING}
                  title="See All Casts"
                  onPress={() => this.RBSheet.open()}
                />
              </View>

              <RBSheet
                ref={ref => {
                  this.RBSheet = ref;
                }}
                height={500}
                openDuration={250}>
                <ScrollView style={styles.containerCast}>
                  <Text style={{padding: 10, fontFamily: 'Montserrat-Bold'}}>
                    All Cast
                  </Text>
                  <View style={styles.bottomSheetCast}>
                    {this.state.dataCast.map(item => (
                      <View key={item.id} style={styles.itemCast}>
                        <Image
                          style={styles.gambarCast}
                          source={{
                            uri: `${item.profile_path}`,
                          }}
                        />
                        <TouchableOpacity
                          onPress={() => {
                            Linking.openURL(
                              `https://www.google.com/search?q=${item.name}`,
                            );
                          }}>
                          <Text style={styles.namaCastSheet}>
                            {item.original_name}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                  </View>
                </ScrollView>
              </RBSheet>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  detailMovie: {
    fontSize: 10,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginVertical: 4,
  },
  otherDetail: {
    marginHorizontal: 20,
    top: -30,
  },
  content: {
    color: WARNA_PUTIH,
    lineHeight: 26,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_KUNING,
    marginBottom: 10,
    marginTop: 10,
  },
  detailKiri: {paddingLeft: 15},
  detailKanan: {marginLeft: 14},
  containerPosterDetail: {
    backgroundColor: WARNA_HITAM,
    borderWidth: 1,
    borderColor: WARNA_KUNING,
    flex: 1,
    flexDirection: 'row',
    top: -50,
    left: 0.05 * windowWidth,
    width: 0.9 * windowWidth,
    borderRadius: 10,
    paddingVertical: 10,
    shadowColor: 'yellow',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
  },
  containerCast: {},
  castName: {
    fontSize: 10,
    color: 'white',
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 4,
  },
  namaCastSheet: {
    fontSize: 12,
    textAlign: 'center',
  },
  itemCast0: {
    width: 75,
    height: 140,
  },
  itemCast: {
    width: 75,
    height: 150,
  },
  gambarCast: {
    width: 75,
    height: 100,
    borderRadius: 4,
  },
  gambarCastBesar: {
    width: 64,
    height: 80,
    borderRadius: 4,
  },
  bottomSheetCast0: {
    flex: 1,
    width: 0.9 * windowWidth,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
  },
  bottomSheetCast: {
    flex: 1,
    width: windowWidth,
    padding: 10,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  namaCast: {
    fontSize: 10,
  },
  cast: {
    flex: 1,

    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  backdropPath: {
    width: windowWidth,
    height: 200,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  headerTitle: {
    backgroundColor: 'white',
    width: windowWidth * 0.8,
    height: 200,
    borderRadius: 5,
    borderColor: 'grey',
    borderWidth: 1,
  },
  gambarFilm: {
    width: 85,
    height: 130,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  container: {
    flex: 1,
    backgroundColor: WARNA_HITAM,
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});
