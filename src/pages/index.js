import Home from './Home';
import Akun from './Akun';
import Splash from './Splash';
import MovieDetail from './MovieDetail';
import List from './List';
import Login from './Login';
export {Home, Akun, Splash, MovieDetail, List,Login};
