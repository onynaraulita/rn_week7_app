import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
  WARNA_ABUGELAP,
} from '../../utils/constant';

import InternetConnectionAlert from 'react-native-internet-connection-alert';

import CompGenres from '../../components/Genres';

import {ProgressiveImage, CompInternetConnectionAlert} from '../../components';
const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};
class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMovie: [],
      isLoading: false,
      isModalVisible: false,
      dataMovies: [],
      dataGenres: [],
      tesid: 497698,
      refreshing: false,
      searchMovie: '',
      newArr: [],
    };
  }

  // onRefresh = React.useCallback(() => {
  //   setRefreshing(true);
  //   wait(2000).then(() => setRefreshing(false));
  // }, []);

  _onRefresh = async () => {
    console.log('refresh');
    this.setState({refreshing: true});
    await axios.get('http://code.aldipee.com/api/v1/movies/');

    this.setState({refreshing: false});

    console.log('berhasil loading');
    // fetchData().then(() => {
    //   this.setState({refreshing: false});
    // });
  };

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_KUNING);
    this.setState({isLoading: true});
    const hasilFetching = await axios.get(
      'http://code.aldipee.com/api/v1/movies/',
    );

    this.setState({
      listMovie: hasilFetching.data.results,
    });

    //console.log(this.state.listMovie);
    this.setState({isLoading: false});
  }

  async getGenres() {
    // const getGenresMovie = await axios.get(
    //   `http://code.aldipee.com/api/v1/movies/${id}}`,
    // );

    // console.log('print: ' + getGenresMovie);

    // this.setState({dataGenres: getGenresMovie.data.genres});
    // console.log('print genre: ' + this.state.dataGenres);

    this.setState({isLoading: true});
    const dataMovieDetailDariServer = await axios.get(
      `http://code.aldipee.com/api/v1/movies/497698`,
    );

    console.log('print: ' + dataMovieDetailDariServer.data);

    this.setState({dataGenres: dataMovieDetailDariServer.data.genres});

    this.setState({isLoading: false});
    console.log('print genre : ' + this.state.dataGenres);
  }

  getSearchResult = searchMovie => {
    this.setState({
      newArr: this.state.listMovie
        .filter(temp => temp.original_title.includes(searchMovie))
        .map(({id, original_title, poster_path}) => ({
          id,
          original_title,
          poster_path,
        })),
    });

    console.log('hasil cari: ' + this.state.newArr);
  };

  render() {
    return (
      <View
        style={{
          padding: 10,
          backgroundColor: WARNA_HITAM,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          {this.state.isLoading ? (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: WARNA_HITAM,
              }}>
              <ActivityIndicator
                size="large"
                color={WARNA_KUNING}
                style={styles.styleLoading}
              />
            </View>
          ) : null}

          <Text style={styles.title}>All Movies</Text>
          <View>
            <TextInput
              style={styles.inputMovies}
              defaultValue=""
              placeholder="Search Movie"
              name="searchMovie"
              onChangeText={text => {
                this.setState({searchMovie: text});
                console.log(this.state.searchMovie);
              }}
            />
            <TouchableOpacity
              onPress={() => {
                this.getSearchResult(this.state.searchMovie);
              }}>
              <Text
                style={{
                  color: WARNA_PUTIH,
                  backgroundColor: WARNA_KUNING,
                  alignSelf: 'flex-start',
                  paddingVertical: 10,
                  paddingHorizontal: 20,
                  borderRadius: 4,
                  marginBottom: 10,
                  borderWidth: 0.5,
                  borderBottomColor: WARNA_KUNING,
                }}>
                Search
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.latestUpload}>
            {this.state.newArr !== null
              ? //bisa index, atau pake map(item), nanti item.id
                this.state.newArr.map((item, index) => (
                  <View key={index} style={styles.movieItem}>
                    <View style={styles.posterLatest}>
                      <Image
                        style={styles.gambarFilm}
                        source={{
                          uri: `${item.poster_path}`,
                        }}
                      />
                    </View>
                    <View style={styles.infoMovie}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat-Bold',
                          color: WARNA_PUTIH,
                        }}>
                        {item.original_title}
                      </Text>

                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate('MovieDetail', {
                            namaFilm: item.id,
                          });
                        }}>
                        <Text style={styles.buttonShowMore}>Show More</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ))
              : null}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default List;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  latestUpload: {
    backgroundColor: WARNA_HITAM,
  },
  inputMovies: {
    backgroundColor: WARNA_PUTIH,
    borderRadius: 6,
    padding: 10,
    marginBottom: 20,
    fontFamily: 'Montserrat-Medium',
    color: WARNA_HITAM,
  },
  buttonShowMore: {
    backgroundColor: WARNA_KUNING,
    alignSelf: 'flex-start',
    color: WARNA_HITAM,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    padding: 6,
    borderRadius: 6,
    marginTop: 10,
  },
  infoMovie: {
    marginLeft: windowWidth * 0.02,
    color: WARNA_PUTIH,
  },
  posterLatest: {},
  recommended: {
    marginBottom: 20,
    color: WARNA_PUTIH,
    padding: 6,
  },
  recommendedItem: {
    marginRight: 5,
  },
  movieItem: {
    padding: 6,
    paddingRight: 6,
    marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    color: WARNA_PUTIH,
    backgroundColor: WARNA_HITAM,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  layanan: {
    paddingLeft: 30,
    padding: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarFilm: {
    width: 120,
    height: 160,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
