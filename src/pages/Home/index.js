import {ImageHeader, Logo} from '../../assets';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
} from '../../utils/constant';

import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {ProgressiveImage, CompInternetConnectionAlert} from '../../components';
import CompGenres from '../../components/Genres';

import NetworkChecker from 'react-native-network-checker';
const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

class Home extends Component {
  constructor(props, username) {
    super(props);
    this.state = {
      listMovie: [],
      isLoading: false,
      isModalVisible: false,
      dataMovies: [],
      dataGenres: {},
      refreshing: false,
      listFavMovies: [],
      username: '',
    };
  }

  _onRefresh = async () => {
    console.log('refresh');
    this.setState({refreshing: true});
    await axios.get('http://code.aldipee.com/api/v1/movies/');
    this.setState({refreshing: false});
    console.log('berhasil refresh');
  };

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_KUNING);
    // const get = this.props.route.params.username;
    // this.setState({username: get});
    // console.log('userlogin: ' + this.state.username);

    this.setState({isLoading: true});
    const hasilFetching = await axios.get(
      'http://code.aldipee.com/api/v1/movies/',
    );

    this.setState({
      listMovie: hasilFetching.data.results,
    });

    //console.log(this.state.listMovie);
    this.setState({isLoading: false});
  }

  getGenres(id) {
    const dataMovieDetailDariServer = axios.get(
      `http://code.aldipee.com/api/v1/movies/${id}`,
    );

    this.setState({dataGenres: dataMovieDetailDariServer.genres});
  }

  render() {
    return (
      <View
        style={{
          padding: 10,
          backgroundColor: WARNA_HITAM,
          color: WARNA_PUTIH,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          {this.state.isLoading ? (
            <View
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: WARNA_HITAM,
              }}>
              <ActivityIndicator size="large" color={WARNA_KUNING} />
            </View>
          ) : null}

          {/* <TouchableOpacity onPress={this._onRefresh}>
              <Icon name="refresh" color={WARNA_UTAMA} />
            </TouchableOpacity> */}
          <Text style={styles.title}>Recommended</Text>
          <View style={styles.recommended}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {this.state.listMovie
                .sort((a, b) => (a.vote_average < b.vote_average ? 1 : -1))
                .map((item, index) => (
                  <View key={index} style={styles.recommendedItem}>
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate('MovieDetail', {
                          namaFilm: item.id,
                        });
                      }}>
                      <View key={index} style={styles.container}>
                        <View>
                          <Image
                            style={styles.gambarFilm}
                            source={{
                              uri: `${item.poster_path}`,
                            }}
                          />
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                ))}
            </ScrollView>
          </View>

          <Text style={styles.title}>Latest Upload</Text>
          <View style={styles.latestUpload}>
            {
              //bisa index, atau pake map(item), nanti item.id
              this.state.listMovie
                .sort((a, b) => (a.release_date > b.release_date ? -1 : 1)) //desc
                .map((item, index) => (
                  <View key={index} style={styles.movieItem}>
                    <View style={styles.posterLatest}>
                      <Image
                        style={styles.gambarFilm}
                        source={{
                          uri: `${item.poster_path}`,
                        }}
                      />
                    </View>
                    <View style={styles.infoMovie}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat-Bold',
                          color: WARNA_PUTIH,
                        }}>
                        {item.original_title}
                      </Text>

                      <Text
                        style={{
                          fontFamily: 'Montserrat-Light',
                          fontSize: 12,
                          color: WARNA_PUTIH,
                          marginTop: 4,
                        }}>
                        {moment(item.release_date).format('DD MMMM YYYY')}
                      </Text>

                      <View
                        style={{flex: 1, flexDirection: 'row', marginTop: 4}}>
                        <Icon name="star" color={WARNA_KUNING} size={16} />
                        <Text
                          style={{
                            fontFamily: 'Montserrat-Regular',
                            fontSize: 12,
                            marginLeft: 6,
                            color: WARNA_PUTIH,
                          }}>
                          {item.vote_average}
                        </Text>
                      </View>
                      <CompGenres id={item.id} />

                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate('MovieDetail', {
                            namaFilm: item.id,
                          });
                        }}>
                        <Text style={styles.buttonShowMore}>Show More</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                ))
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  buttonShowMore: {
    backgroundColor: WARNA_KUNING,
    alignSelf: 'flex-start',
    color: WARNA_HITAM,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    padding: 6,
    borderRadius: 6,
  },
  infoMovie: {
    marginLeft: windowWidth * 0.02,
    color: WARNA_PUTIH,
  },
  posterLatest: {},
  recommended: {
    marginBottom: 20,
    color: WARNA_PUTIH,
    padding: 6,
  },
  recommendedItem: {
    marginRight: 5,
  },
  movieItem: {
    padding: 6,
    paddingRight: 6,
    marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    color: WARNA_PUTIH,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  layanan: {
    paddingLeft: 30,
    padding: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarFilm: {
    width: 120,
    height: 160,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
