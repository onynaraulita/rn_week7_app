import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  LogBox,
  Dimensions,
} from 'react-native';
import {SplashBackground, Logo, LogoUvi} from '../../assets';
import {WARNA_HITAM, WARNA_KUNING} from '../../utils/constant';

const Splash = ({navigation}) => {
  //perintah yg akan dijalankan pertama kali
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login'); //kalo replace, diback akan hilang. kalo navigate, di back akan ke splashscreen
    }, 2000);
  }, [navigation]);

  LogBox.ignoreLogs(['Remote debugger']);
  return (
    <View style={{flex: 1}}>
      <View style={styles.background}>
        <Image source={LogoUvi} style={styles.logo} />
      </View>

      <Text style={styles.namaBawah}>@onikk</Text>
    </View>
  );
};

export default Splash;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  namaBawah: {
    position: 'absolute',
    left: windowWidth * 0.4,
    bottom: windowHeight * 0.01,
    color: WARNA_KUNING,
    fontFamily: 'Montserrat-Bold',
    fontSize: 10,
  },
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: WARNA_HITAM,
  },
  logo: {
    width: 200,
    height: 80,
  },
});
