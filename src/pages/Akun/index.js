import {ImageHeader, Logo} from '../../assets';
import React, {Component} from 'react';
import {
  Dimensions,
  View,
  Text,
  StyleSheet,
  TextInput,
  Alert,
  Button,
  ScrollView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Sort,
} from 'react-native'; //import view dulu
import {Image} from 'react-native-elements';
import moment from 'moment';
import 'moment/locale/id';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {
  WARNA_ABU_ABU,
  WARNA_UTAMA,
  WARNA_HITAM,
  WARNA_KUNING,
  WARNA_PUTIH,
  WARNA_wARNING,
} from '../../utils/constant';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

import {ProgressiveImage, CompInternetConnectionAlert} from '../../components';
import CompGenres from '../../components/Genres';
const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};
class Akun extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMovie: [],
      isLoading: false,
      isModalVisible: false,
      dataMovies: [],
      dataGenres: [],
      tesid: 497698,
      refreshing: false,
    };
  }

  _onRefresh = async () => {
    console.log('refresh');
    this.setState({refreshing: true});
    await axios.get('http://code.aldipee.com/api/v1/movies/');

    this.setState({refreshing: false});

    console.log('berhasil loading');
    // fetchData().then(() => {
    //   this.setState({refreshing: false});
    // });
  };

  async componentDidMount() {
    StatusBar.setBackgroundColor(WARNA_KUNING);
    this.setState({isLoading: true});
    const hasilFetching = await axios.get(
      'http://code.aldipee.com/api/v1/movies/',
    );

    this.setState({
      listMovie: hasilFetching.data.results,
    });

    //console.log(this.state.listMovie);
    this.setState({isLoading: false});
  }

  async getGenres() {
    const dataMovieDetailDariServer = await axios.get(
      `http://code.aldipee.com/api/v1/movies/497698`,
    );

    console.log('print: ' + dataMovieDetailDariServer.data);

    this.setState({dataGenres: dataMovieDetailDariServer.data.genres});

    console.log('print genre : ' + this.state.dataGenres);
  }

  render() {
    return (
      <View style={{padding: 10, backgroundColor: WARNA_HITAM}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          {this.state.isLoading ? (
            <View
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator
                size="large"
                color={WARNA_KUNING}
                style={styles.styleLoading}
              />
            </View>
          ) : null}

          <Text style={styles.title}>Account</Text>
          <TouchableOpacity>
            <Text style={styles.menuTitle}>Log out</Text>
          </TouchableOpacity>
          {/* <Text style={{color: WARNA_PUTIH}}>Favorites Movies</Text>
            <View style={styles.latestUpload}>
              {
                //bisa index, atau pake map(item), nanti item.id
                this.state.listMovie.map((item, index) => (
                  <View key={index} style={styles.movieItem}>
                    <View style={styles.posterLatest}>
                      <Image
                        style={styles.gambarFilm}
                        source={{
                          uri: `${item.poster_path}`,
                        }}
                      />
                    </View>
                    <View style={styles.infoMovie}>
                      <Text
                        style={{
                          fontFamily: 'Montserrat-Bold',
                          color: WARNA_PUTIH,
                        }}>
                        {item.original_title}
                      </Text>
                    </View>
                  </View>
                ))
              }
            </View> */}
        </ScrollView>
      </View>
    );
  }
}

export default Akun;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  menuTitle: {
    color: WARNA_PUTIH,
    fontFamily: 'Montserrat-Regular',
    paddingVertical: 10,
    borderTopWidth: 0.5,
    borderColor: WARNA_PUTIH,
    marginBottom: 10,
  },
  buttonShowMore: {
    backgroundColor: WARNA_KUNING,
    alignSelf: 'flex-start',
    color: WARNA_HITAM,
    fontFamily: 'Montserrat-Medium',
    fontSize: 12,
    padding: 6,
    borderRadius: 6,
  },
  infoMovie: {
    marginLeft: windowWidth * 0.02,
    color: WARNA_PUTIH,
  },
  posterLatest: {},
  recommended: {
    marginBottom: 20,
    color: WARNA_PUTIH,
    padding: 6,
  },
  recommendedItem: {
    marginRight: 5,
  },
  movieItem: {
    padding: 6,
    paddingRight: 6,
    marginVertical: 10,
    flex: 1,
    flexDirection: 'row',
    color: WARNA_PUTIH,
  },
  page: {
    flex: 1,
    padding: 10,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  logo: {
    width: windowWidth * 0.25,
    height: windowHeight * 0.06,
  },
  hello: {
    marginTop: windowHeight * 0.03,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Montserrat-Bold',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  miniTitle: {
    fontSize: 12,
    fontFamily: 'Montserrat-Regular',
    color: WARNA_PUTIH,
    marginBottom: 8,
  },
  username: {
    fontSize: 22,
    fontFamily: 'TitilliumWeb-Bold',
  },
  layanan: {
    paddingLeft: 30,
    padding: 15,
  },
  label: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold',
    paddingBottom: 1,
  },
  garisBawah: {
    borderBottomWidth: 2,
    borderBottomColor: WARNA_UTAMA,
    width: windowWidth * 0.16,
    marginBottom: 5,
  },
  gambarFilm: {
    width: 80,
    height: 110,
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
});
