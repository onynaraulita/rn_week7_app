import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Share,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Home, Splash, Akun, MovieDetail, List, Login} from '../pages/';
import {BottomNavigator, CompHeaderIcon} from '../components/';
import {LogoUvi} from '../assets/';
import {
  WARNA_HITAM,
  WARNA_PUTIH,
  WARNA_KUNING,
  WARNA_ABUGELAP,
  WARNA_ABU_ABU,
} from '../utils/constant';

import Icon from 'react-native-vector-icons/dist/FontAwesome';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Explore" component={List} />
      <Tab.Screen name="Account" component={Akun} />
    </Tab.Navigator>
  );
};

const Router = props => {
  const [isFav, setFav] = React.useState(false);
  const [dataMovieDetail, setDataMovieDetail] = React.useState({});
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `Watch ${dataMovieDetail.original_title} now at MovieApp! See the movie detail at ${dataMovieDetail.homepage}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <Stack.Navigator
      initialRouteName="Splash"
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: {backgroundColor: WARNA_HITAM},
      }}>
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{
          headerStyle: {
            backgroundColor: WARNA_HITAM,
          },
          headerTitle: () => (
            <>
              <Image source={LogoUvi} style={{width: 80, height: 30}} />
            </>
          ),

          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="MovieDetail"
        component={MovieDetail}
        options={{
          // headerStyle: {
          //   backgroundColor: WARNA_HITAM,
          // },
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Detail Movie
            </Text>
          ),
          headerShown: true,
          headerRight: () => (
            <View
              style={{
                flexDirection: 'row',
              }}>
              <CompHeaderIcon />
            </View>
          ),
        }}
      />
      <Stack.Screen
        name="List"
        component={List}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              List Movie
            </Text>
          ),
          headerShown: true,
        }}
      />
      <Stack.Screen
        name="Akun"
        component={Akun}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Akun
            </Text>
          ),
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          headerTitle: () => (
            <Text style={{color: WARNA_PUTIH, fontFamily: 'Montserrat-Bold'}}>
              Login
            </Text>
          ),
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );
};

export default Router;
